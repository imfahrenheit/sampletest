# React , Redux and Docker Implantation 

## Introduction

A very minimal to-do app to demonstrate my ability to use the specified tools while producing clean and maintainable code.

## Code Samples



## Installation

1. This is a private repository with given access to a reviewer. There is also a docker image hosted on docker hub connection to this repository. In my experiment to run an Automated build command from this repository didn't work as it was only throwing errors regarding yarn issue.

2. However,  when you pull the image from docker hub( imfahrenheit/react-redux-test) /  current repo from bitbucket and run the following docker commands it should run.

After pulling the bitbucket repository 

```
docker build -t imfahrenheit/react-redux-test .
```
run this from the project directory.

Then  run
```
docker run -p 3000:5000 imfahrenheit/react-redux-test
```
to get the project running on mapped to http://localhost:3000/

You can also just pull the image from docker hub and run it
