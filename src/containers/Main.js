import React, { Component } from "react";
import MainTable from "../components/MainTable";
import MyButtons from "../components/Buttons";
import { userData } from "../containers/Data";

class Main extends Component {
  state = {
    users: userData,
    sortedBy: ""
  };

  handleSorting = data => {
    this.setState({ users: data });
  };

  sortByAge = () => {
    const users = [...this.state.users];
    users.sort((a, b) => {
      let p = a.age;
      let q = b.age;
      return p < q ? -1 : p > q ? 1 : 0;
    });

    this.setState({ users });
  };

  sortByName = () => {
    const users = [...this.state.users];
    users.sort((a, b) => {
      let p = a.name.toLowerCase();
      let q = b.name.toLowerCase();
      return p < q ? -1 : p > q ? 1 : 0;
    });

    this.setState({ users });
  };

  sortByPoints = () => {
    const users = [...this.state.users];
    users.sort((a, b) => {
      let p = a.points;
      let q = b.points;
      return p < q ? -1 : p > q ? 1 : 0;
    });

    this.setState({ users });
  };

  sortByRank = () => {
    const users = [...this.state.users];
    users.sort((a, b) => {
      let p = a.rank;
      let q = b.rank;
      return p < q ? -1 : p > q ? 1 : 0;
    });

    this.setState({ users });
  };

  render() {
    const { users } = this.state;

    return (
      <div className="container">
      <h1> Leader Board</h1>
        <MyButtons
          sortByName={this.sortByName}
          sortByAge={this.sortByAge}
          sortByPoints={this.sortByPoints}
          sortByRank={this.sortByRank}
        />
        <MainTable users={users} sortByName={this.sortByName} />
      </div>
    );
  }
}

export default Main;
