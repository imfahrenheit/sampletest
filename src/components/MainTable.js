import React from 'react'
import {Table} from 'semantic-ui-react';


const MainTable = ({users}) => {
  
  return <div style= {{margin:"0 auto"}}>
  
    <Table style={{ margin: "auto" }} >
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell>Name</Table.HeaderCell>
            <Table.HeaderCell>Age</Table.HeaderCell>
            <Table.HeaderCell>Point</Table.HeaderCell>
            <Table.HeaderCell>Rank</Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          {users.map(
            (item,i)=> {
              return <Table.Row key={i}>
                  <Table.Cell>{item.name}</Table.Cell>
                  <Table.Cell> {item.age}</Table.Cell>
                <Table.Cell>{item.points}</Table.Cell>
                <Table.Cell>{item.rank}</Table.Cell>
                </Table.Row>;

            })}
        
        </Table.Body>
      </Table>
    </div>;
}

export default MainTable;
