import React from 'react';
import { Button } from 'semantic-ui-react';


const Buttons = ({ sortByName,sortByAge,sortByPoints,sortByRank}) => {
  
  return <div style={{ marginBottom:'20px'}}>
    <Button onClick={sortByName} content="Name" size="tiny" />
      <Button onClick={sortByAge} content="Age"  size="tiny" />
      <Button onClick={sortByPoints} content="Points" size="tiny" />
      <Button onClick={sortByRank} content="Rank"  size="tiny" />
    </div>;
}

export default Buttons
